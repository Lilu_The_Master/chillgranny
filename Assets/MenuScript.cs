﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour
{
    public void PlayGame()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Intructions");

    }

    public void QuitGame()
    {
        Application.Quit();
        Debug.Log("Quit");
    }
}


